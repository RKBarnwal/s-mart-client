import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './components/App'

const SmartApp = () => {
  return <App />
}

ReactDOM.render(<SmartApp />, document.getElementById('smart-app-root'))
