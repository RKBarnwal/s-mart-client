/* eslint-disable react/jsx-filename-extension */
import React from 'react'

export const BannerContext = React.createContext()
export const DealsContext = React.createContext()
export const ProductsContext = React.createContext()
export const FilterContext = React.createContext()
export const SortingContext = React.createContext()
export const CartContext = React.createContext()
