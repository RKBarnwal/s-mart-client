/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { render, screen } from '@testing-library/react'
import { CartContext } from '../../context/smartStore'
import App from './App'

describe('App', () => {
  const cartValue = { cartData: [], updateCartData: () => {} }
  const componentRender = (
    <CartContext.Provider value={cartValue}>
      <App />
    </CartContext.Provider>
  )
  it('should render App component successfully', async () => {
    render(componentRender)

    expect(screen.getByRole('navigation')).toBeInTheDocument()
  })
})
