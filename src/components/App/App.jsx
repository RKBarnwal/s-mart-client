import React, { Suspense, useEffect, useMemo, useState } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { Footer } from '../molecules/Footer'
import Dashboard from '../pages/Dashboard/Dashboard'
import { HeaderWithMenu } from '../organisms/HeaderMenu'
import {
  CART_PAGE,
  CODE_ELECTRONICS,
  CODE_FASHION,
  ELECTRONICS_PAGE,
  FASHION_PAGE,
  PRODUCT_DETAIL_PAGE,
} from '../../utils/constants'
import './appStyle.scss'
import { CartContext } from '../../context/smartStore'
import {
  getCartDataFromSession,
  setValueInSession,
} from '../../utils/smartHelpers'
import { NotFoundPage } from '../pages/NotFoundPage'
import ErrorBoundary from '../organisms/ErrorBoundary/ErrorBoundary'
import ProductDetailView from '../pages/ProductDetailView/ProductDetailView'
import CartView from '../pages/CartView/CartView'

const App = () => {
  const [cartData, updateCartData] = useState(getCartDataFromSession())
  const cartValue = useMemo(() => {
    return { cartData, updateCartData }
  }, [cartData])

  useEffect(() => {
    setValueInSession('cartData', cartData)
  }, [cartData])

  const ProductPageComponent = React.lazy(() =>
    import('../pages/ProductListAndFilterView/ProductListAndFilterView')
  )

  return (
    <ErrorBoundary>
      <CartContext.Provider value={cartValue}>
        <Router>
          <HeaderWithMenu />
          <main className="app-container">
            <Suspense fallback={<div>Loading page...</div>}>
              <Routes>
                <Route path="/" element={<Dashboard />} />
                <Route
                  path={ELECTRONICS_PAGE}
                  element={<ProductPageComponent code={CODE_ELECTRONICS} />}
                />
                <Route
                  path={FASHION_PAGE}
                  element={<ProductPageComponent code={CODE_FASHION} />}
                />
                <Route
                  path={PRODUCT_DETAIL_PAGE}
                  element={<ProductDetailView />}
                />
                <Route path={CART_PAGE} element={<CartView />} />
                <Route path="*" element={<NotFoundPage />} />
              </Routes>
            </Suspense>
          </main>
          <Footer />
        </Router>
      </CartContext.Provider>
    </ErrorBoundary>
  )
}

export default App
