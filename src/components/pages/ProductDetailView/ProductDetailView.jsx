import React from 'react'
import { useLocation } from 'react-router-dom'
import ProductView from '../../molecules/ProductView/ProductView'

const ProductDetailView = () => {
  const { state: productDetails } = useLocation()

  return <ProductView productDetails={productDetails} detailView />
}

export default ProductDetailView
