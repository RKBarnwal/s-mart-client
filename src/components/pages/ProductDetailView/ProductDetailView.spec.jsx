/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { act, render, screen, waitFor } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { CartContext } from '../../../context/smartStore'
import ProductDetailView from './ProductDetailView'

describe('App', () => {
  const mockIntersectionObserver = jest.fn()
  mockIntersectionObserver.mockReturnValue({
    observe: () => null,
    unobserve: () => null,
    disconnect: () => null,
  })
  window.IntersectionObserver = mockIntersectionObserver

  const cartValue = { cartData: [], updateCartData: () => {} }
  const componentRender = (
    <CartContext.Provider value={cartValue}>
      <MemoryRouter
        initialEntries={[
          {
            state: {
              id: 1,
              title: 'WD 2TB Elements Portable External Hard Drive - USB 3.0 ',
              price: { sellingPrice: 64, mrp: 100, discount: 36 },
              description:
                'USB 3.0 and USB 2.0 Compatibility Fast data transfers Improve PC Performance High Capacity; Compatibility Formatted NTFS for Windows 10, Windows 8.1, Windows 7; Reformatting may be required for other operating systems; Compatibility may vary depending on user’s hardware configuration and operating system',
              category: 'electronics',
              image: 'https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg',
              freeDelivery: 'Monday, June6',
              rating: { rate: 3.3, count: 203 },
            },
          },
        ]}
      >
        <ProductDetailView />
      </MemoryRouter>
    </CartContext.Provider>
  )
  it('should render Produt detail component successfully ', async () => {
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getByRole('img')).toBeInTheDocument()
      expect(screen.getByRole('listitem')).toBeInTheDocument()
    })
  })
})
