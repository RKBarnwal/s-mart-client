import React, { useEffect, useMemo, useState } from 'react'
import { BannerContext, DealsContext } from '../../../context/smartStore'
import { fetchBannerData, fetchDealsData } from '../../../utils/apiData'
import { setValueInSession } from '../../../utils/smartHelpers'
import { OfferCarousel } from '../../molecules/Carousel'
import { DealsAlbum } from '../../molecules/ImageGridAlbum'

const Dashboard = () => {
  const [bannerData, updateBannerData] = useState([])
  const [dealsData, updateDealsData] = useState([])

  useEffect(() => {
    const fetchAndSetBannerData = async () => {
      const data = await fetchBannerData()
      updateBannerData(data)
    }
    const fetchAndSetDealsData = async () => {
      const data = await fetchDealsData()
      updateDealsData(data)
    }

    fetchAndSetBannerData()
    fetchAndSetDealsData()

    setValueInSession('filterData', null)
    setValueInSession('code', null)
  }, [])

  const bannerValue = useMemo(() => {
    return {
      bannerData,
      updateBannerData,
    }
  }, [bannerData])
  const dealsValue = useMemo(() => {
    return { dealsData, updateDealsData }
  }, [dealsData])

  return (
    <BannerContext.Provider value={bannerValue}>
      <DealsContext.Provider value={dealsValue}>
        <div className="dashboard-container">
          <OfferCarousel imageList={bannerData} />
          <DealsAlbum imageList={dealsData} />
        </div>
      </DealsContext.Provider>
    </BannerContext.Provider>
  )
}

export default Dashboard
