/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import {
  BannerContext,
  CartContext,
  DealsContext,
} from '../../../context/smartStore'
import bannerData from '../../../../public/Mocks/bannerMock/bannermock.json'
import dealsData from '../../../../public/Mocks/dealsmock/dealsmock.json'
import Dashboard from './Dashboard'

describe('App', () => {
  const cartValue = { cartData: [], updateCartData: () => {} }
  const bannerValue = {
    bannerData,
    updateBannerData: () => {},
  }
  const dealsValue = { dealsData, updateDealsData: () => {} }

  const componentRender = (
    <CartContext.Provider value={cartValue}>
      <BannerContext.Provider value={bannerValue}>
        <DealsContext.Provider value={dealsValue}>
          <MemoryRouter>
            <Dashboard />
          </MemoryRouter>
        </DealsContext.Provider>
      </BannerContext.Provider>
    </CartContext.Provider>
  )
  it('should render Dashboard component sucessfully', async () => {
    render(componentRender)

    screen.debug()
    expect(screen.getByTestId('carousel-viewer')).toBeInTheDocument()
  })
})
