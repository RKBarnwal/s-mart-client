import React, { useEffect, useMemo, useState } from 'react'
import { PropTypes } from 'prop-types'
import {
  FilterContext,
  ProductsContext,
  SortingContext,
} from '../../../context/smartStore'
import { fetchProductList } from '../../../utils/apiData'
import { FilterView } from '../../organisms/FilerView'
import { ProductList } from '../../organisms/ProductListView'
import { ProductSorting } from '../../organisms/ProductSorting'
import {
  getFilterParams,
  getFromSession,
  setValueInSession,
} from '../../../utils/smartHelpers'
import { PaginationView } from '../../organisms/PaginationView'
import { getFilterCategoriesList } from '../../../utils/metadataGenerator'
import { SORT_BY_POPULARITY } from '../../../utils/constants'
import './productListAndFilterViewStyle.scss'

const ProductListAndFilterView = ({ code }) => {
  const [productsList, updateProductsList] = useState([])
  const [filterData, updateFilterData] = useState([])
  const [sortBy, updateSortBy] = useState(SORT_BY_POPULARITY)
  const [curPage, updateCurPage] = useState(1)
  const [pagerInfo, updatePagerInfo] = useState({})

  useEffect(() => {
    const fetchProductsListByCategory = async () => {
      const filterParams = getFilterParams(filterData, sortBy, curPage)
      const { pager, pdata } = await fetchProductList(code, filterParams)
      updateProductsList(pdata)
      updatePagerInfo(pager)
    }
    fetchProductsListByCategory()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filterData, sortBy, code, curPage])

  useEffect(() => {
    if (getFromSession('code') !== code) setValueInSession('filterData', null)
    updateFilterData(getFilterCategoriesList())
    setValueInSession('code', code)
  }, [code])

  const prductsValue = useMemo(() => {
    return { productsList, updateProductsList }
  }, [productsList])
  const filterValue = useMemo(() => {
    return { filterData, updateFilterData }
  }, [filterData])
  const sortValue = useMemo(() => {
    return { sortBy, updateSortBy }
  }, [sortBy])

  return (
    <ProductsContext.Provider value={prductsValue}>
      <FilterContext.Provider value={filterValue}>
        <SortingContext.Provider value={sortValue}>
          <div className="product-page-container">
            <div className="product-page-container__left-rail">
              <FilterView />
            </div>
            <div className="product-page-container__right-rail">
              <ProductSorting sortBy={sortBy} onSortClick={updateSortBy} />
              {productsList?.length ? (
                <ProductList productList={productsList} />
              ) : (
                <div style={{ backgroundColor: '#fff', padding: '1rem' }}>
                  <h4>No matching item found, try modifying filters</h4>
                </div>
              )}
              {pagerInfo?.pages?.length ? (
                <PaginationView
                  pager={pagerInfo}
                  onPageChange={updateCurPage}
                />
              ) : null}
            </div>
          </div>
        </SortingContext.Provider>
      </FilterContext.Provider>
    </ProductsContext.Provider>
  )
}

ProductListAndFilterView.propTypes = {
  code: PropTypes.string.isRequired,
}

export default ProductListAndFilterView
