/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { CartContext } from '../../../context/smartStore'
import CartView from './CartView'

describe('App', () => {
  const itemData = {
    id: 1,
    title: 'WD 2TB Elements Portable External Hard Drive - USB 3.0 ',
    price: { sellingPrice: 64, mrp: 100, discount: 36 },
    description:
      'USB 3.0 and USB 2.0 Compatibility Fast data transfers Improve PC Performance High Capacity; Compatibility Formatted NTFS for Windows 10, Windows 8.1, Windows 7; Reformatting may be required for other operating systems; Compatibility may vary depending on user’s hardware configuration and operating system',
    category: 'electronics',
    image: 'https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg',
    freeDelivery: 'Monday, June6',
    rating: { rate: 3.3, count: 203 },
  }
  const cartValue = { cartData: [], updateCartData: () => {} }
  const componentRender = (
    <CartContext.Provider value={cartValue}>
      <MemoryRouter>
        <CartView />
      </MemoryRouter>
    </CartContext.Provider>
  )
  it('should render cart page successfully, with no items', () => {
    render(componentRender)

    expect(screen.getByTestId('missing-cart-view')).toBeInTheDocument()
  })

  it('should render cart page successfully, with 1 item in cart', async () => {
    cartValue.cartData.push(itemData)
    render(componentRender)

    expect(screen.queryByTestId('missing-cart-view')).not.toBeInTheDocument()
    expect(screen.queryByLabelText('My Cart (1)'))
  })

  it('should remove cart data on remove button click, if data id matches', async () => {
    jest
      .spyOn(cartValue, 'updateCartData')
      .mockImplementation((callback) => callback([itemData]))

    render(componentRender)

    expect(screen.queryByLabelText('My Cart (1)'))
    waitFor(() =>
      fireEvent.click(screen.getByTestId('btn-remove-cart__1__electronics'), {
        item: itemData,
      })
    )
    expect(cartValue.updateCartData).toHaveBeenCalled()
  })

  it('should not remove cart data on remove button click, when data id does not matches', async () => {
    jest
      .spyOn(cartValue, 'updateCartData')
      .mockImplementation((callback) =>
        callback([{ category: 'test', id: 123 }])
      )

    render(componentRender)

    expect(screen.queryByLabelText('My Cart (1)'))
    waitFor(() =>
      fireEvent.click(screen.getByTestId('btn-remove-cart__1__electronics'), {
        item: itemData,
      })
    )
    expect(cartValue.updateCartData).toHaveBeenCalled()
  })
})
