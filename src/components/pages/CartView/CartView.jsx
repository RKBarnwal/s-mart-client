import React, { useContext } from 'react'
import { CartContext } from '../../../context/smartStore'
import { AmountSummary } from '../../organisms/AmountSummary'
import { CartItems } from '../../organisms/CartItemsView'
import './CartViewStyle.scss'

const CartView = () => {
  const { cartData, updateCartData } = useContext(CartContext)
  const removeItemFromCart = (item) => {
    updateCartData((cartItems) => {
      return cartItems.filter((it) => {
        if (it.category !== item.category && it.id !== item.id) return true
        return false
      })
    })
  }

  return (
    <div className="cart-view-wrapper">
      {cartData?.length ? (
        <>
          <CartItems cartData={cartData} removeFromCart={removeItemFromCart} />
          <AmountSummary cartData={cartData} />
        </>
      ) : (
        <div className="missing-cart-wrapper" data-testid="missing-cart-view">
          <div style={{ textAlign: 'center' }}>
            <img alt="missing cart items" src="/assets/missing-cart.png" />
            <p>Add an item to view in cart</p>
            <p>For now check Items in Electronics and Fashion sections</p>
          </div>
        </div>
      )}
    </div>
  )
}

export default CartView
