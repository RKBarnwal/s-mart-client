/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { CartContext } from '../../../context/smartStore'
import NotFoundPage from './NotFoundPage'

describe('App', () => {
  const cartValue = { cartData: [], updateCartData: () => {} }
  const componentRender = (
    <CartContext.Provider value={cartValue}>
      <MemoryRouter>
        <NotFoundPage />
      </MemoryRouter>
    </CartContext.Provider>
  )
  it('should render Not found page when invalid route provided ', async () => {
    render(componentRender)

    expect(screen.getByTestId('not-found-container')).toBeInTheDocument()
  })
})
