import React from 'react'
import { Link } from 'react-router-dom'
import { ELECTRONICS_PAGE, FASHION_PAGE } from '../../../utils/constants'
import './notFoundPageStyle.scss'

const NotFoundPage = () => {
  return (
    <div className="not-found-wrapper" data-testid="not-found-container">
      <div style={{ textAlign: 'center', margin: '10rem' }}>
        <img
          alt="page not found"
          src="/assets/page-not-found.png"
          className="not-found-wrapper__image"
        />
        <h3>
          Work in progress, please try our products from&nbsp;
          <Link to={ELECTRONICS_PAGE}>
            <strong>ELECTRONICS</strong>
            &nbsp;
          </Link>
          and&nbsp;
          <Link to={FASHION_PAGE}>
            <strong>FASHION</strong>
          </Link>
          &nbsp; sections
        </h3>
      </div>
    </div>
  )
}

export default NotFoundPage
