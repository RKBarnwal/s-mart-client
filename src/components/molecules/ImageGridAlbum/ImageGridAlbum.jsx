import React from 'react'
import PropTypes from 'prop-types'
import './imageGridAlbumStyle.scss'

const ImageGridAlbum = ({ imageList }) => {
  return (
    <div className="album-container">
      {imageList.map((image) => {
        return (
          <img
            key={image.id}
            alt={image.title}
            src={image.url}
            className="album-container__item"
          />
        )
      })}
    </div>
  )
}

ImageGridAlbum.propTypes = {
  imageList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      url: PropTypes.string,
    })
  ).isRequired,
}

export default ImageGridAlbum
