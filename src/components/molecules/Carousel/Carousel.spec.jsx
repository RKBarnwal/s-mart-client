/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import Carousel from './Carousel'

describe('Carousel', () => {
  jest.useFakeTimers()
  const imageListMock = [
    { id: 1, title: 'deal 1', url: './Mocks/dealsmock/deal1.webp' },
    { id: 2, title: 'deal 2', url: './Mocks/dealsmock/deal2.webp' },
    { id: 3, title: 'deal 3', url: './Mocks/dealsmock/deal3.webp' },
  ]

  it('should rrender Carousel component successfully with no images', async () => {
    render(<Carousel imageList={[]} />)

    expect(screen.getByTestId('carousel-viewer')).toBeInTheDocument()
    expect(screen.queryByRole('img')).not.toBeInTheDocument()
    expect(screen.queryByRole('button')).not.toBeInTheDocument()
  })

  it('should navigate onclick of next and previous button when more than 1 image provided', async () => {
    render(<Carousel imageList={imageListMock} />)
    jest.advanceTimersByTime(3000)

    expect(screen.getByTestId('carousel-viewer')).toBeInTheDocument()
    fireEvent.click(screen.getAllByRole('button')[0])
    fireEvent.click(screen.getAllByRole('button')[1])
    expect(screen.getAllByRole('img')).toHaveLength(3)
    expect(screen.getAllByRole('button')).toHaveLength(2)
  })

  it('should navigate onclick of next and set first immage visibility if no next element found', async () => {
    render(<Carousel imageList={imageListMock} />)

    expect(screen.getByTestId('carousel-viewer')).toBeInTheDocument()
    fireEvent.click(screen.getAllByRole('button')[0])
    fireEvent.click(screen.getAllByRole('button')[0])
    fireEvent.click(screen.getAllByRole('button')[0])
    expect(screen.getAllByRole('img')).toHaveLength(3)
    expect(screen.getAllByRole('button')).toHaveLength(2)
  })

  it('should navigate onclick of previous and set first immage visibility if no previous element found', async () => {
    render(<Carousel imageList={imageListMock} />)

    expect(screen.getByTestId('carousel-viewer')).toBeInTheDocument()
    fireEvent.click(screen.getAllByRole('button')[1])
    fireEvent.click(screen.getAllByRole('button')[1])
    expect(screen.getAllByRole('img')).toHaveLength(3)
    expect(screen.getAllByRole('button')).toHaveLength(2)
  })
})
