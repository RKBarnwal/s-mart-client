import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { BsChevronRight, BsChevronLeft } from 'react-icons/bs'
import './carouselStyle.scss'

export const CarouselItem = ({ children, width }) => {
  return (
    <div className="carousel-item" style={{ width }}>
      {children}
    </div>
  )
}

CarouselItem.propTypes = {
  children: PropTypes.element.isRequired,
  width: PropTypes.string.isRequired,
}
const Carousel = ({ imageList }) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const updateIndex = (idx) => {
    let newIndex = idx
    if (newIndex < 0) {
      newIndex = imageList.length - 1
    } else if (newIndex >= imageList.length) {
      newIndex = 0
    }
    setActiveIndex(newIndex)
  }

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (imageList.length > 1) {
      const carouselInterval = setInterval(() => {
        updateIndex(activeIndex + 1)
      }, 3000)
      return () => {
        /* istanbul ignore else */
        if (carouselInterval) clearInterval(carouselInterval)
      }
    }
  })

  return (
    <div className="carousel-viewer" data-testid="carousel-viewer">
      <div
        className="carousel-viewer__container"
        style={{ transform: `translateX(-${activeIndex * 100}%)` }}
      >
        {imageList.map((image) => {
          return (
            <CarouselItem width="100%" key={image.id}>
              <img
                alt={image.title}
                src={image.url}
                className="carousel-item__image"
              />
            </CarouselItem>
          )
        })}
      </div>
      {imageList.length > 1 ? (
        <>
          <button
            type="button"
            className="carousel-viewer__button-left"
            onClick={() => updateIndex(activeIndex + 1)}
            aria-label="next product banner"
          >
            <BsChevronRight />
          </button>
          <button
            type="button"
            className="carousel-viewer__button-right"
            onClick={() => updateIndex(activeIndex - 1)}
            aria-label="previous product banner"
          >
            <BsChevronLeft />
          </button>
        </>
      ) : null}
    </div>
  )
}

Carousel.propTypes = {
  imageList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      url: PropTypes.string,
    })
  ).isRequired,
}

export default Carousel
