/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useCallback, useContext, useEffect, useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'
import { BsCartDash } from 'react-icons/bs'
import { Price } from '../../atoms/PriceView'
import { Rating } from '../../atoms/StarRating'
import './productViewStyle.scss'
import { PRODUCT_DETAIL_PAGE } from '../../../utils/constants'
import Button from '../../atoms/Button/Button'
import { CartContext } from '../../../context/smartStore'

const ProductView = ({ productDetails, detailView, className }) => {
  const {
    freeDelivery,
    title,
    description,
    price: { sellingPrice, mrp, discount },
    image,
    rating: { rate, count },
  } = productDetails

  const imageRef = useRef(null)
  const navigate = useNavigate()
  const { updateCartData } = useContext(CartContext)

  /* istanbul ignore next */
  const imageObserver = useCallback((img) => {
    const imageObserve = new IntersectionObserver((entries) => {
      entries.forEach((en) => {
        if (en.isIntersecting) {
          const curImg = en.target
          curImg.src = curImg.dataset.src
          curImg.classList.remove('lazy')
          imageObserve?.unobserve(curImg)
        }
      })
    })
    imageObserve.observe(img)
  }, [])

  useEffect(() => {
    if (imageRef.current) imageObserver(imageRef.current)
  }, [imageRef, imageObserver])

  const navigateToProductDetails = () => {
    navigate(PRODUCT_DETAIL_PAGE, { state: productDetails })
  }

  const addToCart = () => {
    updateCartData((itemList) => {
      return [...itemList, productDetails]
    })
  }

  return (
    <li
      className={`product-container ${className} ${
        detailView ? 'product-detail-view' : ''
      }`}
      onClick={navigateToProductDetails}
      onKeyDown={() => {}}
    >
      <img
        alt={title}
        data-src={image}
        ref={imageRef}
        className={`lazy ${
          detailView ? 'product-detail-view__image' : 'product-container__image'
        }`}
      />
      <div
        className={`product-container__details-container ${
          detailView ? 'product-detail-view__details-container' : ''
        }`}
      >
        <p className="product-container__details-container__title">{title}</p>
        {detailView && <p>{description}</p>}
        <Rating rating={rate} count={count} />
        <Price
          priceDetails={{ sellingPrice, mrp, discount }}
          className="product-container__details-container__price"
        />
        {freeDelivery && (
          <p className="product-container__delivery">
            Free delivery by &nbsp;
            <strong>{freeDelivery}</strong>
          </p>
        )}
        {detailView && (
          <Button
            id="btn-add-cart"
            label="Add to cart"
            classname="product-detail-view__details-container__button"
            onClick={addToCart}
          >
            <BsCartDash />
          </Button>
        )}
      </div>
    </li>
  )
}

ProductView.propTypes = {
  productDetails: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    price: PropTypes.shape({
      sellingPrice: PropTypes.number,
      mrp: PropTypes.number,
      discount: PropTypes.number,
    }),
    description: PropTypes.string,
    category: PropTypes.string,
    image: PropTypes.string,
    freeDelivery: PropTypes.string,
    rating: PropTypes.shape({
      rate: PropTypes.number,
      count: PropTypes.number,
    }),
  }).isRequired,
  className: PropTypes.string,
  detailView: PropTypes.bool,
}

ProductView.defaultProps = {
  className: '',
  detailView: false,
}

export default ProductView
