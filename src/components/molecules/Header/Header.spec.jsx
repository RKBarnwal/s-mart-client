/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { Header } from '.'
import { CartContext } from '../../../context/smartStore'

describe('Header', () => {
  const cartValue = { cartData: [], updateCartData: () => {} }
  const renderComponent = (
    <CartContext.Provider value={cartValue}>
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    </CartContext.Provider>
  )
  it('should render Header component successfully with search box and login button', () => {
    render(renderComponent)

    fireEvent.click(screen.getByRole('img'))
    fireEvent.click(screen.queryByTestId('search-button'))
    fireEvent.change(screen.getByRole('textbox'), { target: { value: '123' } })
    expect(screen.getByText('Login')).toBeInTheDocument()
    expect(screen.getByRole('textbox')).toBeInTheDocument()
  })
})
