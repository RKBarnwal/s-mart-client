/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '../../atoms/Button/Button'
import { SearchBox } from '../../atoms/SearchBox'
import CartButton from '../CartButton/CartButton'
import './headerStyle.scss'

const Header = () => {
  const [searchValue, setSearchValue] = useState('')
  const updateSearchValue = ({ target: { value } }) => setSearchValue(value)
  const navigate = useNavigate()
  return (
    <header className="header-container">
      <img
        src="./assets/smart-logo.png"
        alt="page logo"
        className="header-container__sm-logo"
        onClick={() => {
          navigate('/')
        }}
      />
      <SearchBox
        id="itemSearch"
        value={searchValue}
        onChange={updateSearchValue}
        onSearchClick={() => {}}
      />
      <Button
        id="login"
        label="Login"
        classname="header-container__sm-login-button"
      />
      <CartButton className="header-container__sm-login-button" />
    </header>
  )
}

export default Header
