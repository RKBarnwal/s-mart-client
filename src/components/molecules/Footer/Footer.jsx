import React from 'react'
import './footerStyle.scss'

const Footer = () => {
  return <footer className="footer-container">&copy; SMart.com</footer>
}

export default Footer
