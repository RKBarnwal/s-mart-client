import React from 'react'
import { render, screen } from '@testing-library/react'
import { Footer } from '.'

describe('Footer', () => {
  it('should render footer component successfully', () => {
    render(<Footer />)

    expect(screen.getByText('© SMart.com')).toBeInTheDocument()
  })
})
