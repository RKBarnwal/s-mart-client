import React, { useContext } from 'react'
import { BsCartDash } from 'react-icons/bs'
import { PropTypes } from 'prop-types'
import { CartContext } from '../../../context/smartStore'
import Button from '../../atoms/Button/Button'
import './CartButtonStyles.scss'
import { CART_PAGE } from '../../../utils/constants'

const CartButton = ({ className }) => {
  const { cartData } = useContext(CartContext)
  return (
    <Button
      id="cart"
      label="Cart"
      classname={`cart-button ${className}`}
      type="link"
      linkTo={CART_PAGE}
      showCount={cartData.length}
    >
      <BsCartDash />
    </Button>
  )
}

CartButton.propTypes = {
  className: PropTypes.string,
}

CartButton.defaultProps = {
  className: '',
}
export default CartButton
