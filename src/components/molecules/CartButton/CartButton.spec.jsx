/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { act, render, screen, waitFor } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { CartContext } from '../../../context/smartStore'
import CartButton from './CartButton'

describe('App', () => {
  const mockProductList = [
    {
      id: 1,
      title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
      price: 109.95,
      description:
        'Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday',
      category: "men's clothing",
      image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
      rating: { rate: 3.9, count: 120 },
    },
  ]

  it('should render CartButton component successfully with no count if no product in cart', async () => {
    await act(() =>
      render(
        <CartContext.Provider value={{ cartData: [] }}>
          <MemoryRouter>
            <CartButton />
          </MemoryRouter>
        </CartContext.Provider>
      )
    )

    await waitFor(() => {
      expect(screen.getByRole('button')).toBeInTheDocument()
      expect(screen.queryByTestId('cart-count')).not.toBeInTheDocument()
    })
  })

  it('should render CartButton component successfully with count if some product in cart', async () => {
    await act(() =>
      render(
        <CartContext.Provider value={{ cartData: mockProductList }}>
          <MemoryRouter>
            <CartButton />
          </MemoryRouter>
        </CartContext.Provider>
      )
    )

    await waitFor(() => {
      expect(screen.getByRole('button')).toBeInTheDocument()
      expect(screen.queryByTestId('cart-count')).toBeInTheDocument()
    })
  })
})
