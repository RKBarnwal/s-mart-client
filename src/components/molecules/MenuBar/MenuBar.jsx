import React from 'react'
import { Link } from 'react-router-dom'
import { ELECTRONICS_PAGE, FASHION_PAGE } from '../../../utils/constants'
import './menuBarStyle.scss'

const MenuBar = () => {
  return (
    <nav className="sm-menu-bar">
      <Link to="/grocery" className="sm-menu-bar__item">
        <h5>Grocery</h5>
      </Link>
      <Link to="/mobile" className="sm-menu-bar__item">
        <h5>Mobile</h5>
      </Link>
      <Link to="/homeFurniture" className="sm-menu-bar__item">
        <h5>Home & Furniture</h5>
      </Link>
      <Link to={ELECTRONICS_PAGE} className="sm-menu-bar__item">
        <h5>Electronics</h5>
      </Link>
      <Link to="/appliances" className="sm-menu-bar__item">
        <h5>Appliances</h5>
      </Link>
      <Link to={FASHION_PAGE} className="sm-menu-bar__item">
        <h5>Fashion</h5>
      </Link>
      <Link to="/more" className="sm-menu-bar__item">
        <h5>Beauty, Toys & More</h5>
      </Link>
    </nav>
  )
}

export default MenuBar
