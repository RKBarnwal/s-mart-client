import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { SelectedFilter } from '../../atoms/SelectedFilterView'
import './selectedFilterListStyle.scss'
import { FilterContext } from '../../../context/smartStore'

const SelectedFilterList = ({ onClearClick }) => {
  const { filterData } = useContext(FilterContext)
  return (
    <div className="selected-filter-container">
      {filterData?.map((filter) =>
        filter.options?.map((option) => {
          if (option.selected) {
            return (
              <SelectedFilter
                key={`${filter.id}_${filter.category}_${option.label}`}
                type="option"
                category={filter.category}
                label={option.label}
                onClearClick={onClearClick}
              />
            )
          }
          return null
        })
      )}
    </div>
  )
}

SelectedFilterList.propTypes = {
  onClearClick: PropTypes.func.isRequired,
}

export default SelectedFilterList
