/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react'
import { PropTypes } from 'prop-types'
import { Price } from '../../atoms/PriceView'
import './CartItemsStyle.scss'
import Button from '../../atoms/Button/Button'

const CartItemsView = ({ cartData, removeFromCart }) => {
  return (
    <div className="cart-items-container">
      <h3>My Cart &nbsp;({cartData?.length})</h3>
      <div>
        {cartData?.map((item) => (
          <div
            className="item-wrapper"
            key={`${item.id}__${item.category}__${item.price.mrp}`}
          >
            <img
              alt={item.title}
              src={item.image}
              className="item-wrapper__image"
            />
            <div className="item-wrapper__detail">
              <p className="item-wrapper__detail__title">{item.title}</p>
              <Price
                priceDetails={{
                  sellingPrice: item.price?.sellingPrice,
                  mrp: item.price?.mrp,
                  discount: item.price?.discount,
                }}
              />
              {item.freeDelivery && (
                <p>
                  Free delivery by &nbsp;
                  <strong>{item.freeDelivery}</strong>
                </p>
              )}
              <Button
                id={`btn-remove-cart__${item.id}__${item.category}`}
                label="REMOVE"
                classname="item-wrapper__detail__button"
                onClick={() => removeFromCart(item)}
              />
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

CartItemsView.propTypes = {
  cartData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  removeFromCart: PropTypes.func.isRequired,
}

export default CartItemsView
