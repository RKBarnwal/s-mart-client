import React from 'react'
import { PropTypes } from 'prop-types'
import './paginationStyle.scss'

const PaginationView = ({ pager, onPageChange }) => {
  return (
    <div className="pagination-container" data-testid="pagination-view">
      {pager.currentPage !== 1 && (
        <button
          type="button"
          onClick={() => onPageChange(pager.currentPage - 1)}
          className="pagination-container__page-link pagination-container__np-link"
          aria-label={`go to page ${pager.currentPage - 1} of ${
            pager.totalPages
          }`}
        >
          Previous
        </button>
      )}
      {pager.pages?.map((page) => (
        <button
          key={`page_${page}`}
          type="button"
          onClick={() => onPageChange(page)}
          className={`pagination-container__page-link ${
            pager.currentPage === page
              ? 'pagination-container__page-link--active'
              : ''
          }`}
          aria-label={`${
            pager.currentPage === page
              ? `page number ${page} is active`
              : `go to page ${page} of ${pager.totalPages}`
          }`}
        >
          {page}
        </button>
      ))}
      {pager.currentPage !== pager.totalPages && (
        <button
          type="button"
          onClick={() => onPageChange(pager.currentPage + 1)}
          className="pagination-container__page-link pagination-container__np-link"
          aria-label={`go to page ${pager.currentPage + 1} of ${
            pager.totalPages
          }`}
        >
          Next
        </button>
      )}
    </div>
  )
}

PaginationView.propTypes = {
  pager: PropTypes.shape({
    currentPage: PropTypes.number,
    endIndex: PropTypes.number,
    endPage: PropTypes.number,
    pageSize: PropTypes.number,
    pages: PropTypes.arrayOf(PropTypes.number),
    startIndex: PropTypes.number,
    startPage: PropTypes.number,
    totalItems: PropTypes.number,
    totalPages: PropTypes.number,
  }).isRequired,
  onPageChange: PropTypes.func.isRequired,
}

export default React.memo(PaginationView)
