/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import PaginationView from './PaginationView'

describe('App', () => {
  const paginationMock = {
    pager: {
      totalItems: 150,
      currentPage: 1,
      pageSize: 10,
      totalPages: 15,
      startPage: 1,
      endPage: 10,
      startIndex: 0,
      endIndex: 9,
      pages: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    },
    onPageChange: () => {},
  }
  const componentRender = (
    <MemoryRouter>
      <PaginationView
        pager={paginationMock.pager}
        onPageChange={paginationMock.onPageChange}
      />
    </MemoryRouter>
  )
  it('should render pagination component successfully with next button only when on page 1', async () => {
    render(componentRender)

    expect(screen.getByText('Next')).toBeInTheDocument()
    expect(screen.queryByText('Previous')).not.toBeInTheDocument()
  })

  it('should render pagination component successfully with next and previous buttons when on page 2', async () => {
    paginationMock.pager.currentPage = 2
    render(componentRender)

    fireEvent.click(screen.getByText('Next'))
    fireEvent.click(screen.getByText('Previous'))
    fireEvent.click(screen.getByText('3'))
    expect(screen.getByText('Next')).toBeInTheDocument()
    expect(screen.queryByText('Previous')).toBeInTheDocument()
    expect(screen.getByText('3')).toBeInTheDocument()
  })

  it('should render pagination component successfully with previous button only when on last page', async () => {
    paginationMock.pager.currentPage = 10
    render(componentRender)

    expect(screen.getByText('Next')).toBeInTheDocument()
    expect(screen.queryByText('Previous')).toBeInTheDocument()
  })
})
