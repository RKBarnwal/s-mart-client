/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { FilterContext } from '../../../context/smartStore'
import { getFilterCategoriesList } from '../../../utils/metadataGenerator'
import FilterView from './FilterView'

describe('App', () => {
  const selectedFiltersDataMock = [
    {
      id: 1,
      category: 'rating',
      title: 'Customer Ratings',
      isSingleSelect: true,
      options: [
        { label: '4 &#9733; & above', value: 4, selected: false },
        { label: '3 &#9733; & above', value: 3, selected: true },
        { label: '2 &#9733; & above', value: 2, selected: false },
        { label: '1 &#9733; & above', value: 1, selected: false },
      ],
    },
    {
      id: 2,
      category: 'price',
      title: 'Price',
      isSingleSelect: false,
      options: [
        { label: 'under 500', value: '1-500', selected: true },
        {
          label: '&#8377;500 - &#8377;1000',
          value: '500-1000',
          selected: true,
        },
        {
          label: '&#8377;1000 - &#8377;5000',
          value: '1000-5000',
          selected: false,
        },
        {
          label: '&#8377;5000 - &#8377;10000',
          value: '5000-10000',
          selected: false,
        },
      ],
    },
  ]

  const filterValue = {
    filterData: getFilterCategoriesList(),
    updateFilterData: (cb) => cb(selectedFiltersDataMock),
  }

  const componentRender = (
    <FilterContext.Provider value={filterValue}>
      <MemoryRouter>
        <FilterView />
      </MemoryRouter>
    </FilterContext.Provider>
  )
  it('should render FilterView component successfully with given categories data ', async () => {
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
      expect(screen.getByText('Price')).toBeInTheDocument()
      expect(screen.queryByText('CLEAR ALL')).toBeInTheDocument()
      expect(screen.queryByText('Clear all')).not.toBeInTheDocument()
    })
  })

  it('should render FilterView component successfully with selected filters', async () => {
    filterValue.filterData = selectedFiltersDataMock
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
      expect(screen.queryAllByText('Clear all')).toHaveLength(2)
    })
  })

  it('should update filterData on remove selected filter category', async () => {
    filterValue.filterData = selectedFiltersDataMock
    jest.spyOn(filterValue, 'updateFilterData')
    await act(() => render(componentRender))

    await waitFor(() => {
      const clearAllBtn = screen.queryAllByText('Clear all')[0]
      fireEvent.click(clearAllBtn)
      expect(filterValue.updateFilterData).toHaveBeenCalled()
    })
  })

  it('should update filterData on remove selected filter', async () => {
    filterValue.filterData = selectedFiltersDataMock
    jest.spyOn(filterValue, 'updateFilterData')
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getAllByRole('button')).toHaveLength(4)
      const removeBtn = screen.getAllByRole('button')[1]
      fireEvent.click(removeBtn)
      expect(filterValue.updateFilterData).toHaveBeenCalled()
    })
  })

  it('should update and reset filterData when clear all button clicked for clearing all selected filters', async () => {
    filterValue.filterData = selectedFiltersDataMock
    jest.spyOn(filterValue, 'updateFilterData')
    await act(() => render(componentRender))

    await waitFor(() => {
      fireEvent.click(screen.queryByText('CLEAR ALL'))
      expect(filterValue.updateFilterData).toHaveBeenCalled()
    })
  })

  it('should update filterData and deselect selected filter', async () => {
    filterValue.filterData = selectedFiltersDataMock
    jest.spyOn(filterValue, 'updateFilterData')
    await act(() => render(componentRender))

    await waitFor(() => {
      fireEvent.change(screen.getByTestId('1__rating__3'))
      expect(filterValue.updateFilterData).toHaveBeenCalled()
    })
  })

  it('should update filterData and select selected filter for muliselect type', async () => {
    filterValue.filterData = selectedFiltersDataMock
    jest.spyOn(filterValue, 'updateFilterData')
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(selectedFiltersDataMock[1].options[3].selected).toBeFalsy()
      fireEvent.click(screen.getByTestId('2__price__5000-10000'), {
        target: { value: '5000-10000' },
      })
      expect(filterValue.updateFilterData).toHaveBeenCalled()
      expect(selectedFiltersDataMock[1].options[3].selected).toBeTruthy()
    })
  })
})
