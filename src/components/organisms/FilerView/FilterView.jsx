/* eslint-disable no-param-reassign */
import React, { useContext } from 'react'
import { FilterContext } from '../../../context/smartStore'
import { setValueInSession } from '../../../utils/smartHelpers'
import { SelectedFilterList } from '../../molecules/SelectedFilterList'
import { FilterCategoryList } from '../FilterCategoryList'
import './filterViewStyle.scss'

const FilterView = () => {
  const { filterData, updateFilterData } = useContext(FilterContext)

  const removeSelectedFilter = ({ target }, type) => {
    const selFilterId = target.closest('button')?.id
    if (selFilterId) {
      const splittedVal = selFilterId.split('__')
      const selCategory = splittedVal[0]
      const selValue = splittedVal[1]

      updateFilterData((oldFilterData) => {
        const newFilterData = [...oldFilterData]
        newFilterData
          .find((filter) => filter.category === selCategory)
          ?.options.forEach((option) => {
            if (type === 'category') {
              option.selected = false
            } else if (type === 'option' && option.label === selValue) {
              option.selected = false
            }
          })
        setValueInSession('filterData', newFilterData)

        return newFilterData
      })
    }
  }
  const isCategorySelected = (category) =>
    filterData
      ?.find((filter) => filter.category === category)
      ?.options.some((option) => option.selected)

  const toggleSlectedFilterList = ({ target: { id: selId } }, type) => {
    if (type === 'all') {
      updateFilterData((oldFilterData) => {
        const newFilterData = [...oldFilterData]
        newFilterData.forEach((category) => {
          category.options.forEach((option) => {
            option.selected = false
          })
        })
        setValueInSession('filterData', null)

        return newFilterData
      })
    }
    if (selId) {
      const splittedValue = selId.split('__')
      const selCategory = splittedValue[1]
      const selValue = splittedValue[2]

      updateFilterData((oldFilterData) => {
        const newFilterData = [...oldFilterData]
        const filterCategory = newFilterData.find(
          (filter) => filter.category === selCategory
        )
        if (!filterCategory.isSingleSelect) {
          filterCategory?.options.forEach((option) => {
            if (option.value === selValue) {
              option.selected = !option.selected
            }
          })
        } else {
          filterCategory?.options.forEach((option) => {
            option.selected = option.value === +selValue
          })
        }
        setValueInSession('filterData', newFilterData)

        return newFilterData
      })
    }
  }

  return (
    <div className="filter-container">
      <div className="filter-container__header">
        <h2 className="filter-container__header__title">Filters</h2>
        {filterData?.length ? (
          <button
            type="button"
            className="filter-container__header__clear-button"
            onClick={(e) => toggleSlectedFilterList(e, 'all')}
          >
            CLEAR ALL
          </button>
        ) : null}
      </div>
      <SelectedFilterList onClearClick={removeSelectedFilter} />
      <FilterCategoryList
        onOptionClick={toggleSlectedFilterList}
        onCategoryRemove={removeSelectedFilter}
        isCategorySelected={isCategorySelected}
      />
    </div>
  )
}

export default FilterView
