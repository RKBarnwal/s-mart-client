import React from 'react'
import PropTypes from 'prop-types'
import ProductView from '../../molecules/ProductView/ProductView'
import './productListViewStyle.scss'

const ProductListView = ({ productList }) => {
  return (
    <ul className="product-list-container">
      {productList?.map((product) => {
        return (
          <ProductView
            key={`${product.category}_${product.id}`}
            className="product-list-container__item"
            productDetails={product}
          />
        )
      })}
    </ul>
  )
}

ProductListView.propTypes = {
  productList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      price: PropTypes.shape({
        sellingPrice: PropTypes.number,
        mrp: PropTypes.number,
        discount: PropTypes.number,
      }),
      description: PropTypes.string,
      category: PropTypes.string,
      image: PropTypes.string,
      freeDelivery: PropTypes.string,
      rating: PropTypes.shape({
        rate: PropTypes.number,
        count: PropTypes.number,
      }),
    })
  ).isRequired,
}

export default ProductListView
