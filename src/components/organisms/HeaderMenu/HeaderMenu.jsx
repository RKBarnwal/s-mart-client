import React from 'react'
import Header from '../../molecules/Header/Header'
import { Menu } from '../../molecules/MenuBar'

const HeaderMenu = () => {
  return (
    <div>
      <Header />
      <Menu />
    </div>
  )
}

export default HeaderMenu
