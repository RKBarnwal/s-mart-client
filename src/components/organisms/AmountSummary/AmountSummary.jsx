/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react'
import { PropTypes } from 'prop-types'
import './AmountSummaryStyle.scss'

const AmountSummary = ({ cartData }) => {
  const price = cartData?.reduce((sum, item) => {
    return sum + item.price.sellingPrice
  }, 0)
  const discount = cartData?.reduce((sum, item) => {
    return sum + item.price.discount
  }, 0)
  const total = price + discount

  return (
    <div className="cart-price-container">
      <h3>Amount Summary</h3>
      <div className="cart-price-container__detail">
        <div className="cart-price-container__detail__item">
          <span>Price</span>
          <span>&#8377; {price}</span>
        </div>
        <div className="cart-price-container__detail__item">
          <span>Discount</span>
          <span>&#8377; {discount}</span>
        </div>
        <div className="cart-price-container__detail__item">
          <span>Delivery Chnares</span>
          <span>FREE</span>
        </div>
        <div className="cart-price-container__detail__item">
          <span>Secured Packaging Fee</span>
          <span>&#8377; 29</span>
        </div>
        <hr className="break" />
        <div className="cart-price-container__detail__item total">
          <span>Total</span>
          <span>&#8377; {total}</span>
        </div>
        <hr className="break" />
      </div>
    </div>
  )
}

AmountSummary.propTypes = {
  cartData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
}

export default AmountSummary
