import React from 'react'
import { render, screen } from '@testing-library/react'
import AmountSummary from './AmountSummary'

describe('App', () => {
  const itemData = {
    id: 1,
    title: 'WD 2TB Elements Portable External Hard Drive - USB 3.0 ',
    price: { sellingPrice: 64, mrp: 100, discount: 36 },
    description:
      'USB 3.0 and USB 2.0 Compatibility Fast data transfers Improve PC Performance High Capacity; Compatibility Formatted NTFS for Windows 10, Windows 8.1, Windows 7; Reformatting may be required for other operating systems; Compatibility may vary depending on user’s hardware configuration and operating system',
    category: 'electronics',
    image: 'https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg',
    freeDelivery: 'Monday, June6',
    rating: { rate: 3.3, count: 203 },
  }

  it('should render account summary component successfully ', async () => {
    render(<AmountSummary cartData={[itemData]} />)

    expect(screen.getByText('Amount Summary')).toBeInTheDocument()
    expect(screen.getByText('Secured Packaging Fee')).toBeInTheDocument()
    expect(screen.getByText('Total')).toBeInTheDocument()
  })
})
