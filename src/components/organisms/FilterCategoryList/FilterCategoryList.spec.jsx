/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { FilterContext } from '../../../context/smartStore'
import FilterCategoryList from './FilterCategoryList'

describe('App', () => {
  const selectedFiltersDataMock = [
    {
      id: 1,
      category: 'rating',
      title: 'Customer Ratings',
      isSingleSelect: true,
      options: [
        { label: '4 &#9733; & above', value: 4, selected: false },
        { label: '3 &#9733; & above', value: 3, selected: true },
        { label: '2 &#9733; & above', value: 2, selected: false },
        { label: '1 &#9733; & above', value: 1, selected: false },
      ],
    },
    {
      id: 2,
      category: 'price',
      title: 'Price',
      isSingleSelect: false,
      options: [
        { label: 'under 500', value: '1-500', selected: true },
        {
          label: '&#8377;500 - &#8377;1000',
          value: '500-1000',
          selected: true,
        },
        {
          label: '&#8377;1000 - &#8377;5000',
          value: '1000-5000',
          selected: false,
        },
        {
          label: '&#8377;5000 - &#8377;10000',
          value: '5000-10000',
          selected: false,
        },
      ],
    },
  ]

  const filterValue = {
    filterData: selectedFiltersDataMock,
    updateFilterData: (cb) => cb(selectedFiltersDataMock),
  }

  const mockFunctions = {
    onOptionClick: jest.fn(),
    onCategoryRemove: jest.fn(),
    isCategorySelected: () => {
      return true
    },
  }

  const componentRender = (
    <FilterContext.Provider value={filterValue}>
      <MemoryRouter>
        <FilterCategoryList
          onOptionClick={mockFunctions.onOptionClick}
          onCategoryRemove={mockFunctions.onCategoryRemove}
          isCategorySelected={mockFunctions.isCategorySelected}
        />
      </MemoryRouter>
    </FilterContext.Provider>
  )
  it('should render FilterCategoryList component successfully with given categories data ', async () => {
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
      expect(screen.getByText('Price')).toBeInTheDocument()
    })
  })

  it('should render FilterView component successfully with selected filters', async () => {
    await act(() => render(componentRender))

    await waitFor(() => {
      expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
      expect(screen.queryAllByText('Clear all')).toHaveLength(2)
    })
  })

  it('should update filterData on remove selected filter category', async () => {
    jest.spyOn(mockFunctions, 'onCategoryRemove')
    await act(() => render(componentRender))
    const clearBtnList = screen.queryAllByText('Clear all')
    expect(clearBtnList).toHaveLength(2)
    fireEvent.change(clearBtnList[0])
    expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
  })

  it('should update filterData and deselect selected filter', async () => {
    jest.spyOn(mockFunctions, 'onOptionClick')
    await act(() => render(componentRender))
    const checkbox = screen.getByTestId('1__rating__3')
    expect(checkbox).toBeChecked()
    fireEvent.change(screen.getByTestId('1__rating__3'))
    expect(screen.getByText('Customer Ratings')).toBeInTheDocument()
  })
})
