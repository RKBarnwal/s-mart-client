import React, { useContext } from 'react'
import { PropTypes } from 'prop-types'
import { SelectedFilter } from '../../atoms/SelectedFilterView'
import './filterCategoryListStyle.scss'
import { FilterContext } from '../../../context/smartStore'

const FilterCategory = ({
  onOptionClick,
  onCategoryRemove,
  isCategorySelected,
}) => {
  const { filterData } = useContext(FilterContext)
  return (
    <div className="filter-category-wrapper">
      {filterData?.map((filter) => (
        <div key={`filter_${filter.id}`} className="category-container">
          <div className="category-container__header">
            <h3 className="category-container__header__title">
              {filter.title}
            </h3>
          </div>
          <div className="category-view">
            {isCategorySelected(filter.category) && (
              <SelectedFilter
                type="category"
                category={filter.category}
                label="Clear all"
                onClearClick={onCategoryRemove}
                className="category-view__clear-button"
              />
            )}
            <ul className="category-view__wrapper">
              {filter.options.map((option) => {
                return (
                  <li
                    className="category-view__wrapper__item"
                    key={`${filter.category}__${filter.id}_${option.label}`}
                  >
                    <input
                      type={`${filter.isSingleSelect ? 'radio' : 'checkbox'}`}
                      id={`${filter.id}__${filter.category}__${option.value}`}
                      data-testid={`${filter.id}__${filter.category}__${option.value}`}
                      name={`${filter.category}`}
                      className="category-view__wrapper__item__checkbox"
                      onChange={onOptionClick}
                      checked={option.selected}
                      value={option.value}
                      aria-label={`${filter.category} ${option.label}`}
                    />
                    <span
                      className="category-view__wrapper__item__value"
                      aria-hidden
                      // eslint-disable-next-line react/no-danger
                      dangerouslySetInnerHTML={{ __html: option.label }}
                    />
                  </li>
                )
              })}
            </ul>
          </div>
        </div>
      ))}
    </div>
  )
}

FilterCategory.propTypes = {
  onOptionClick: PropTypes.func.isRequired,
  onCategoryRemove: PropTypes.func.isRequired,
  isCategorySelected: PropTypes.func.isRequired,
}

export default React.memo(FilterCategory)
