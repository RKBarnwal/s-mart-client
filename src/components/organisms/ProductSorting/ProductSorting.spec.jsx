/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import ProductSorting from './ProductSorting'
import {
  SORT_BY_POPULARITY,
  SORT_BY_PRICE_ASC,
  SORT_BY_PRICE_DESC,
} from '../../../utils/constants'

describe('App', () => {
  const componentRender = (sortBy, cb) => (
    <MemoryRouter>
      <ProductSorting sortBy={sortBy} onSortClick={cb} />
    </MemoryRouter>
  )

  it('should render ProductSorting component successfully', async () => {
    render(componentRender(SORT_BY_POPULARITY, () => {}))

    expect(screen.getByText('Popularity')).toBeInTheDocument()
    expect(screen.getByText('Price -- Low to High')).toBeInTheDocument()
    expect(screen.getByText('Price -- High to Low')).toBeInTheDocument()
  })

  it('should call onSortClick function when any sorting option clicked', async () => {
    render(componentRender(SORT_BY_POPULARITY, () => {}))

    fireEvent.click(screen.getByText('Price -- Low to High'), {
      target: { value: SORT_BY_PRICE_ASC },
    })
    expect(screen.getByText('Popularity')).toBeInTheDocument()
    expect(screen.getByText('Price -- Low to High')).toBeInTheDocument()
    expect(screen.getByText('Price -- High to Low')).toBeInTheDocument()
  })

  it('should render ProductSorting component successfully with price low-to-high selected', async () => {
    render(componentRender(SORT_BY_PRICE_ASC, () => {}))

    expect(screen.getByText('Popularity')).toBeInTheDocument()
    expect(screen.getByText('Price -- Low to High')).toBeInTheDocument()
    expect(screen.getByText('Price -- High to Low')).toBeInTheDocument()
  })

  it('should render ProductSorting component successfully with price high-low-selected', async () => {
    render(componentRender(SORT_BY_PRICE_DESC, () => {}))

    expect(screen.getByText('Popularity')).toBeInTheDocument()
    expect(screen.getByText('Price -- Low to High')).toBeInTheDocument()
    expect(screen.getByText('Price -- High to Low')).toBeInTheDocument()
  })
})
