import React from 'react'
import { PropTypes } from 'prop-types'
import {
  SORT_BY_POPULARITY,
  SORT_BY_PRICE_ASC,
  SORT_BY_PRICE_DESC,
} from '../../../utils/constants'
import './productSortingStyle.scss'

const ProductSorting = ({ sortBy, onSortClick }) => {
  const updateSelectedOption = ({ target: { value } }) => {
    onSortClick(value)
  }
  return (
    <div className="sorting-option-container">
      <strong>Sort By</strong>
      <div className="sorting-option-container__options">
        <button
          type="button"
          id="option1"
          className={`sorting-option-container__options--option ${
            sortBy === SORT_BY_POPULARITY ? 'selected-option' : ''
          }`}
          value={SORT_BY_POPULARITY}
          onClick={updateSelectedOption}
        >
          Popularity
        </button>
        <button
          type="button"
          id="option2"
          className={`sorting-option-container__options--option ${
            sortBy === SORT_BY_PRICE_ASC ? 'selected-option' : ''
          }`}
          value={SORT_BY_PRICE_ASC}
          onClick={updateSelectedOption}
        >
          Price -- Low to High
        </button>
        <button
          type="button"
          id="option3"
          className={`sorting-option-container__options--option ${
            sortBy === SORT_BY_PRICE_DESC ? 'selected-option' : ''
          }`}
          value={SORT_BY_PRICE_DESC}
          onClick={updateSelectedOption}
        >
          Price -- High to Low
        </button>
      </div>
    </div>
  )
}

ProductSorting.propTypes = {
  sortBy: PropTypes.string.isRequired,
  onSortClick: PropTypes.func.isRequired,
}

export default React.memo(ProductSorting)
