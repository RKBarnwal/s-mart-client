/* eslint-disable react/button-has-type */
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './buttonStyle.scss'

const Button = ({
  id,
  type,
  classname,
  label,
  onClick,
  children,
  childPos,
  showCount,
  linkTo,
}) => {
  const renderButton = () => {
    return (
      <button
        id={`button__${id}`}
        data-testid={id}
        type={type}
        className={`sm-button ${classname}`}
        onClick={onClick}
      >
        {childPos === 'left' ? (
          <>
            {children}
            <span className="sm-button__label-right">{label}</span>
          </>
        ) : (
          <>
            <span className="sm-button__label-left">{label}</span>
            {children}
          </>
        )}
        {showCount ? (
          <span className="sm-button__count" data-testid="cart-count">
            {showCount}
          </span>
        ) : null}
      </button>
    )
  }

  return (
    <div>
      {type === 'link' ? (
        <Link to={linkTo}>{renderButton()}</Link>
      ) : (
        renderButton()
      )}
    </div>
  )
}

Button.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  classname: PropTypes.string,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  children: PropTypes.element,
  childPos: PropTypes.string,
  showCount: PropTypes.number,
  linkTo: PropTypes.string,
}

Button.defaultProps = {
  type: 'button',
  classname: '',
  children: null,
  childPos: 'left',
  onClick: () => {},
  showCount: 0,
  linkTo: '',
}

export default Button
