import React from 'react'
import PropTypes from 'prop-types'
import { BsSearch } from 'react-icons/bs'
import './searchBoxStyle.scss'

const SearchBox = ({ id, classname, value, onChange, onSearchClick }) => {
  return (
    <div className="sm-search-box-container">
      <input
        type="text"
        id={`search_${id}`}
        className={`sm-search-box-container__search-box ${classname}`}
        value={value}
        onChange={onChange}
        aria-label="search a product on page"
      />
      <button
        type="button"
        onClick={onSearchClick}
        className="sm-search-box-container__search-button"
        aria-label="search product"
        data-testid="search-button"
      >
        <BsSearch />
      </button>
    </div>
  )
}

SearchBox.propTypes = {
  id: PropTypes.string.isRequired,
  classname: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSearchClick: PropTypes.func.isRequired,
}

SearchBox.defaultProps = {
  classname: '',
  value: '',
}

export default SearchBox
