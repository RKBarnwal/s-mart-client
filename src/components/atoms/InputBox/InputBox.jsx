import React from 'react'
import { PropTypes } from 'prop-types'
import './inputBoxStyle.scss'

const InputBox = (props) => {
  const { id, name, type, label, className, value, onChange } = props

  return (
    <div className="form-control">
      <label
        id={`label__${id}`}
        htmlFor={`input__${id}`}
        className="form-control__label"
      >
        {label}
      </label>
      <input
        id={`input__${id}`}
        data-testid={`input__${id}`}
        name={name}
        type={type}
        className={`form-control__input ${className}`}
        value={value}
        onChange={onChange}
      />
    </div>
  )
}

InputBox.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

InputBox.defaultProps = {
  name: '',
  className: '',
}

export default InputBox
