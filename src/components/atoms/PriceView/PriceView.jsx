import React from 'react'
import PropTypes from 'prop-types'
import './priceViewStyle.scss'

const PriceView = ({ priceDetails }) => {
  const { sellingPrice, mrp, discount } = priceDetails
  return (
    <label
      className="price-container"
      aria-label={`product selling at ${sellingPrice} with discount of ${discount} percentage from mrp ${mrp}`}
    >
      <div className="price-container__sell">
        <span>&#8377;</span>
        <strong>{sellingPrice}</strong>
      </div>
      {sellingPrice !== mrp ? (
        <div className="price-container__mrp">{mrp}</div>
      ) : null}
      {discount ? (
        <div className="price-container__discount">{`${discount}% off`}</div>
      ) : null}
    </label>
  )
}

PriceView.propTypes = {
  priceDetails: PropTypes.shape({
    sellingPrice: PropTypes.number,
    mrp: PropTypes.number,
    discount: PropTypes.number,
  }).isRequired,
}

export default PriceView
