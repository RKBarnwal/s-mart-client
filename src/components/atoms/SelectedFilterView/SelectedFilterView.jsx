import React from 'react'
import { BsX } from 'react-icons/bs'
import PropTypes from 'prop-types'
import './selectedFilterViewStyle.scss'

const SelectedFilterView = ({
  type,
  category,
  label,
  onClearClick,
  className,
}) => {
  return (
    <button
      type="button"
      className={`selected-filter-button ${className}`}
      id={`${category}__${label}`}
      onClick={(e) => onClearClick(e, type)}
    >
      <span className="selected-filter-button__icon">
        <BsX />
      </span>
      <span
        className="selected-filter-button__title"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: label }}
      />
    </button>
  )
}

SelectedFilterView.propTypes = {
  type: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClearClick: PropTypes.func.isRequired,
  className: PropTypes.string,
}

SelectedFilterView.defaultProps = {
  className: '',
}

export default SelectedFilterView
