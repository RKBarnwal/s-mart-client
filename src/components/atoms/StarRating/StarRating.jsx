import React from 'react'
import PropTypes from 'prop-types'
import './starRatingStyle.scss'

const StarRating = ({ rating, count }) => {
  return (
    <label
      className="rating-container"
      aria-label={`product ${rating} stars rated by ${count} persons`}
    >
      <div className="rating-container__ratings">
        <span>{rating}</span>
        <span className="rating-container__ratings--star">&#9733;</span>
      </div>
      <div className="rating-container__rating-count">
        <span>{count}</span>
        &nbsp;
        <span>ratings</span>
      </div>
    </label>
  )
}

StarRating.propTypes = {
  rating: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
}

export default StarRating
