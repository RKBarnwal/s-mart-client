const BASE_PATH = 'http://localhost:3000'

export const fetchBannerData = async () => {
  try {
    const result = await fetch(`${BASE_PATH}/smart/offerbannerlist`)
    return await result.json()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  }
  return []
}

export const fetchDealsData = async () => {
  try {
    const result = await fetch(`${BASE_PATH}/smart/offerdealslist`)
    return await result.json()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  }
  return []
}

export const fetchProductList = async (pcode, reqBody = {}) => {
  try {
    const result = await fetch(`${BASE_PATH}/smart/products/${pcode}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(reqBody),
    })
    return await result.json()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  }
  return []
}
