import { getFromSession } from './smartHelpers'

export const getFilterCategoriesList = () => {
  const sessionFilterData = getFromSession('filterData')
  return (
    sessionFilterData || [
      {
        id: 1,
        category: 'rating',
        title: 'Customer Ratings',
        isSingleSelect: true,
        options: [
          { label: '4 &#9733; & above', value: 4, selected: false },
          { label: '3 &#9733; & above', value: 3, selected: false },
          { label: '2 &#9733; & above', value: 2, selected: false },
          { label: '1 &#9733; & above', value: 1, selected: false },
        ],
      },
      {
        id: 2,
        category: 'price',
        title: 'Price',
        isSingleSelect: false,
        options: [
          { label: 'under 500', value: '1-500', selected: false },
          {
            label: '&#8377;500 - &#8377;1000',
            value: '500-1000',
            selected: false,
          },
          {
            label: '&#8377;1000 - &#8377;5000',
            value: '1000-5000',
            selected: false,
          },
          {
            label: '&#8377;5000 - &#8377;10000',
            value: '5000-10000',
            selected: false,
          },
        ],
      },
    ]
  )
}
