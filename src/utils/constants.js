export const SORT_BY_POPULARITY = 'popularity'
export const SORT_BY_PRICE_ASC = 'price_low_to_high'
export const SORT_BY_PRICE_DESC = 'price_high_to_low'

export const ELECTRONICS_PAGE = '/electronics'
export const FASHION_PAGE = '/fashion'
export const CART_PAGE = '/cart'
export const PRODUCT_DETAIL_PAGE = '/product-detail'

export const CODE_ELECTRONICS = 'electronics'
export const CODE_FASHION = 'fashion'
