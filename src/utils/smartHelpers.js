export const getFilterParams = (filterData, sortBy, pageNo) => {
  const result = []
  filterData.forEach((filter) => {
    const selected = filter.options.filter((option) => option.selected)
    if (selected.length)
      result.push({
        type: filter.category,
        selValues: selected.map((s) => s.value),
      })
  })
  return { filterParams: result, sortBy, pageNo }
}

export const setValueInSession = (key, value) => {
  if (!value) {
    sessionStorage.removeItem(key)
  } else {
    sessionStorage.setItem(key, JSON.stringify(value))
  }
}

export const getFromSession = (key) => {
  const k = sessionStorage.getItem(key)
  return k ? JSON.parse(k) : null
}

export const getCartDataFromSession = () => {
  const data = sessionStorage.cartData
  return data ? JSON.parse(data) : []
}
