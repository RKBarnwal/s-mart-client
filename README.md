# S-Mart - Shop Smart

### Description

Holds code for React frontend application. This is a sample learning application based on React, webpack 5.

#### Implementation Details

- Using React-router-dom v6 for routing between pages
- Used code-splitting and lazy-loading of routes with React.lazy
- Added fallback with Suspense tags
- Added ErrorBoundary for handling rendering errors
- Lazy loaded images with use of IntersectionObserver
- Server-side pagination
- Server-side filtering
- Server-side sorting
- Used husky hooks for pre-commit, pre-push
- webpack bundeler used with lot of configurations - eslint, stylelint, code coverage, scss, commitlint etc.
- React Testing Library used for unit testing of application
- Code coverage > 90%

#### Interactions

- For now Electronics and Fashion routes are working
- For all other routes work-in-progress page will appear
- Cart feature is limited to 1 pc/item. Adding more can cause unwanted behaviour of application
- Login, Search features are in progress
- static code analysis - eslint, style lint, code coverage, commitlint

### Running Application

- Start client app with `npm start` command. Client will run on `localhost:8080`
